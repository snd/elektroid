# elektroid Brazilian Portuguese translation
# Copyright (C) 2021 David García Goñi
# This file is distributed under the same license as the elektroid package.
# Gustavo Costa <xfgusta@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: elektroid 2.2\n"
"Report-Msgid-Bugs-To: dagargo@gmail.com\n"
"POT-Creation-Date: 2023-03-18 23:23+0100\n"
"PO-Revision-Date: 2023-03-26 11:30-0300\n"
"Last-Translator: Gustavo Costa <xfgusta@gmail.com>\n"
"Language-Team: \n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: src/connector.c:38
msgid "MIDI device"
msgstr "Dispositivo MIDI"

#: src/elektroid.c:465 src/elektroid.c:3923
msgid "Not connected"
msgstr "Não conectado"

#: src/elektroid.c:615
msgid "Cancelling..."
msgstr "Cancelando..."

#: src/elektroid.c:651 src/elektroid.c:2419 src/elektroid.c:2629
#: src/elektroid.c:3337
msgid "Waiting..."
msgstr "Esperando..."

#: src/elektroid.c:654
msgid "Sending..."
msgstr "Enviando..."

#: src/elektroid.c:657
msgid "Receiving..."
msgstr "Recebendo..."

#: src/elektroid.c:738
msgid "Receive SysEx"
msgstr "Receber SysEx"

#: src/elektroid.c:765
msgid "Save SysEx"
msgstr "Salvar SysEx"

#: src/elektroid.c:768 src/elektroid.c:899 src/elektroid.c:1094
#: src/elektroid.c:2086 res/gui.glade:1593 res/gui.glade:1693
msgid "_Cancel"
msgstr "_Cancelar"

#: src/elektroid.c:770 res/gui.glade:1607
msgid "_Save"
msgstr "_Salvar"

#: src/elektroid.c:774
msgid "Received SysEx"
msgstr "SysEx recebido"

#: src/elektroid.c:779 src/elektroid.c:905
msgid "SysEx Files"
msgstr "Arquivos SysEx"

#: src/elektroid.c:813
#, c-format
msgid "Error while saving “%s”: %s."
msgstr "Erro ao salvar \"%s\": %s."

#: src/elektroid.c:835
#, c-format
msgid "Error while loading “%s”: %s."
msgstr "Erro ao carregar \"%s\": %s."

#: src/elektroid.c:896
msgid "Open SysEx"
msgstr "Abrir SysEx"

#: src/elektroid.c:901
msgid "_Open"
msgstr "_Abrir"

#: src/elektroid.c:917
msgid "Sending SysEx"
msgstr "Enviando SysEx"

#: src/elektroid.c:1093
msgid "Are you sure you want to delete the selected items?"
msgstr "Tem certeza de que deseja excluir os itens selecionados?"

#: src/elektroid.c:1095
msgid "_Delete"
msgstr "_Excluir"

#: src/elektroid.c:1107
msgid "Deleting Files"
msgstr "Excluindo arquivos"

#: src/elektroid.c:1108
msgid "Deleting..."
msgstr "Excluindo..."

#: src/elektroid.c:1138 res/gui.glade:123 res/gui.glade:214
msgid "Rename"
msgstr "Renomar"

#: src/elektroid.c:1161
#, c-format
msgid "Error while renaming to “%s”: %s."
msgstr "Erro ao renomear para \"%s\": %s."

#: src/elektroid.c:1664 res/gui.glade:535 res/gui.glade:764
msgid "Add Directory"
msgstr "Adicionar diretório"

#: src/elektroid.c:1682
#, c-format
msgid "Error while creating dir “%s”: %s."
msgstr "Erro ao criar diretório \"%s\": %s."

#: src/elektroid.c:1754
msgid "Queued"
msgstr "Na fila"

#: src/elektroid.c:1756
msgid "Running"
msgstr "Executando"

#: src/elektroid.c:1758
msgid "Completed"
msgstr "Completado"

#: src/elektroid.c:1760
msgid "Terminated with errors"
msgstr "Encerrado com erros"

#: src/elektroid.c:1762
msgid "Canceled"
msgstr "Cancelado"

#: src/elektroid.c:1764 src/elektroid.c:1778
msgid "Undefined"
msgstr "Indefinido"

#: src/elektroid.c:1774
msgid "Upload"
msgstr "Upload"

#: src/elektroid.c:1776
msgid "Download"
msgstr "Download"

#: src/elektroid.c:2083
#, c-format
msgid "Replace file “%s”?"
msgstr "Substituir o arquivo \"%s\"?"

#: src/elektroid.c:2087
msgid "_Skip"
msgstr "_Pular"

#: src/elektroid.c:2088
msgid "_Replace"
msgstr "_Substituir"

#: src/elektroid.c:2092
msgid "Apply this action to all files"
msgstr "Aplicar esta ação a todos os arquivos"

#: src/elektroid.c:2418 src/elektroid.c:2628 src/elektroid.c:3336
msgid "Preparing Tasks"
msgstr "Preparando tarefas"

#: src/elektroid.c:3026
msgid "Connecting to Device"
msgstr "Conectando ao dispositivo"

#: src/elektroid.c:3027
msgid "Connecting..."
msgstr "Conectando..."

#: src/elektroid.c:3036
#, c-format
msgid "Device “%s” not recognized: %s"
msgstr "Dispositivo \"%s\" não reconhecido: %s"

#: src/elektroid.c:3322
msgid "Moving Files"
msgstr "Movendo arquivos"

#: src/elektroid.c:3323
msgid "Moving..."
msgstr "Movendo..."

#: src/editor.c:104
msgid "minutes"
msgstr "minutos"

#: src/menu_actions/backend.c:60
msgid "OS _Upgrade"
msgstr "Atualizar _OS"

#: src/menu_actions/backend.c:74
msgid "_Receive SysEx"
msgstr "_Receber SysEx"

#: src/menu_actions/backend.c:88
msgid "_Send SysEx"
msgstr "_Enviar SysEx"

#: src/menu_actions/microbrute.c:392
msgid "_Configuration"
msgstr "_Configuração"

#: src/menu_actions/microbrute.c:419
msgid "_Calibration"
msgstr "_Calibração"

#: src/local.c:484
msgid "System"
msgstr "Sistema"

#: src/connectors/sds.c:1124
msgid "SDS sampler"
msgstr "Sampler SDS"

#: res/gui.glade:72
msgid "Upload Selection"
msgstr "Seleção de upload"

#: res/gui.glade:87 res/gui.glade:178
msgid "Play"
msgstr "Reproduzir"

#: res/gui.glade:101 res/gui.glade:192
msgid "Open With External Editor"
msgstr "Abrir com um editor externo"

#: res/gui.glade:109 res/gui.glade:200
msgid "Show in File Manager"
msgstr "Mostrar no gerenciador de arquivos"

#: res/gui.glade:132 res/gui.glade:223
msgid "Delete"
msgstr "Excluir"

#: res/gui.glade:163
msgid "Download Selection"
msgstr "Seleção de download"

#: res/gui.glade:263
msgid "Sample and MIDI device manager"
msgstr "Gerenciador de amostras e dispositivos MIDI"

#: res/gui.glade:266
msgid "translator-credits"
msgstr "Gustavo Costa <xfgusta@gmail.com>"

#: res/gui.glade:329
msgid "_About"
msgstr "_Sobre"

#: res/gui.glade:410
msgid "Refresh Devices"
msgstr "Atualizar dispositivos"

#: res/gui.glade:514 res/gui.glade:743
msgid "Go to Parent Directory"
msgstr "Ir para o diretório pai"

#: res/gui.glade:556 res/gui.glade:785
msgid "Refresh Directory"
msgstr "Atualizar diretório"

#: res/gui.glade:639 res/gui.glade:846 res/gui.glade:1638
msgid "Name"
msgstr "Nome"

#: res/gui.glade:654 res/gui.glade:860
msgid "Size"
msgstr "Tamanho"

#. It is recommended to split the text in two lines if it is too long
#: res/gui.glade:963
msgid "Auto play"
msgstr ""
"Reprodução\n"
"automática"

#: res/gui.glade:1003
msgid "Playing mix depends on the remote channels"
msgstr "A reprodução da mixagem depende dos canais remotos"

#. It is recommended to split the text in two lines if it is too long
#: res/gui.glade:1005
msgid ""
"Mix depending\n"
"on remote"
msgstr ""
"Mixagem dependendo\n"
"do destino"

#: res/gui.glade:1195
msgid "Samples"
msgstr "Amostras"

#: res/gui.glade:1209
msgid "Channels"
msgstr "Canais"

#: res/gui.glade:1223
msgid "Sample rate"
msgstr "Taxa de amostragem"

#: res/gui.glade:1237
msgid "Bit depth"
msgstr "Profundidade de bit"

#: res/gui.glade:1295
msgid "Duration"
msgstr "Duração"

#: res/gui.glade:1361
msgid "Status"
msgstr "Status"

#: res/gui.glade:1372
msgid "Type"
msgstr "Tipo"

#: res/gui.glade:1393
msgid "Source"
msgstr "Origem"

#: res/gui.glade:1407
msgid "Destination"
msgstr "Destino"

#: res/gui.glade:1422
msgid "Progress"
msgstr "Progresso"

#: res/gui.glade:1454
msgid "Cancel Tasks"
msgstr "Cancelar tarefas"

#: res/gui.glade:1475
msgid "Remove Queued Tasks"
msgstr "Remover tarefas em fila"

#: res/gui.glade:1496
msgid "Clear Finished Tasks"
msgstr "Limpar tarefas concluídas"

#: res/microbrute/gui.glade:35
msgid "Calibration assistant"
msgstr "Assistente de calibração"

#: res/microbrute/gui.glade:46
msgid "This procedure calibrates the pitch bend and modulation wheels."
msgstr "Este procedimento calibra os controles de pitch bend e modulação."

#: res/microbrute/gui.glade:50
msgid "Start"
msgstr "Começo"

#: res/microbrute/gui.glade:63
msgid ""
"Let the pitch bend wheel rest at the neutral position and click on the next "
"button."
msgstr ""
"Deixe o controle de pitch bend na posição neutra e clique no próximo botão."

#: res/microbrute/gui.glade:68
msgid "Step 1"
msgstr "Passo 1"

#: res/microbrute/gui.glade:81
msgid "While setting both wheels at the bottom click on the next button."
msgstr ""
"Ao posicionar ambos os controles na posição mais baixa, clique no próximo "
"botão."

#: res/microbrute/gui.glade:86
msgid "Step 2"
msgstr "Passo 2"

#: res/microbrute/gui.glade:99
msgid "While setting both wheels at the top click on the next button."
msgstr ""
"Ao posicionar ambos os controles na posição mais alta, clique no próximo "
"botão."

#: res/microbrute/gui.glade:104
msgid "Step 3"
msgstr "Passo 3"

#: res/microbrute/gui.glade:117
msgid "Calibration completed"
msgstr "Calibração completada"

#: res/microbrute/gui.glade:122
msgid "End"
msgstr "Fim"

#: res/microbrute/gui.glade:145
msgctxt "Gate Length"
msgid "Short"
msgstr "Curta"

#: res/microbrute/gui.glade:149
msgctxt "Gate Length"
msgid "Medium"
msgstr "Média"

#: res/microbrute/gui.glade:153
msgctxt "Gate Length"
msgid "Long"
msgstr "Longa"

#: res/microbrute/gui.glade:167
msgctxt "Key Priority"
msgid "Last"
msgstr "Última"

#: res/microbrute/gui.glade:171
msgctxt "Key Priority"
msgid "Low"
msgstr "Baixa"

#: res/microbrute/gui.glade:175
msgctxt "Key Priority"
msgid "High"
msgstr "Alta"

#: res/microbrute/gui.glade:189
msgctxt "Play Mode"
msgid "Hold"
msgstr "Manter"

#: res/microbrute/gui.glade:193
msgctxt "Play Mode"
msgid "Note On"
msgstr "Nota ativa"

#: res/microbrute/gui.glade:207
msgctxt "Receive Channel"
msgid "Any"
msgstr "Qualquer um"

#: res/microbrute/gui.glade:285
msgctxt "Sequence Change"
msgid "At End"
msgstr "No fim"

#: res/microbrute/gui.glade:289
msgctxt "Sequence Change"
msgid "Instant Reset"
msgstr "Reinício instantâneo"

#: res/microbrute/gui.glade:293
msgctxt "Sequence Change"
msgid "Instant Continuation"
msgstr "Continuação instantânea"

#: res/microbrute/gui.glade:307
msgctxt "Sequence Retrigger"
msgid "Reset"
msgstr "Reiniciar"

#: res/microbrute/gui.glade:311
msgctxt "Sequence Retrigger"
msgid "Reset But Legato"
msgstr "Reiniciar (com legato)"

#: res/microbrute/gui.glade:315
msgctxt "Sequence Retrigger"
msgid "None"
msgstr "Nenhum"

#: res/microbrute/gui.glade:355
msgctxt "Step On"
msgid "Clock"
msgstr "Clock"

#: res/microbrute/gui.glade:359
msgctxt "Step On"
msgid "Gate"
msgstr "Gate"

#: res/microbrute/gui.glade:373
msgctxt "Synchronization"
msgid "Auto"
msgstr "Automática"

#: res/microbrute/gui.glade:377
msgctxt "Synchronization"
msgid "Internal"
msgstr "Interna"

#: res/microbrute/gui.glade:381
msgctxt "Synchronization"
msgid "External"
msgstr "Externa"

#: res/microbrute/gui.glade:469
msgctxt "Velocity Response"
msgid "Linear"
msgstr "Linear"

#: res/microbrute/gui.glade:473
msgctxt "Velocity Response"
msgid "Logarithmic"
msgstr "Logarítmica"

#: res/microbrute/gui.glade:477
msgctxt "Velocity Response"
msgid "Exponential"
msgstr "Exponencial"

#: res/microbrute/gui.glade:484
msgid "MicroBrute Configuration"
msgstr "Configuração do MicroBrute"

#: res/microbrute/gui.glade:508
msgid "Persistent changes"
msgstr "Alterações persistentes"

#: res/microbrute/gui.glade:575
msgid "Transmit Channel"
msgstr "Canal de transmissão"

#: res/microbrute/gui.glade:587
msgid "Receive Channel"
msgstr "Canal de recepção"

#: res/microbrute/gui.glade:678
msgid "Key Priority"
msgstr "Prioridade de tecla"

#: res/microbrute/gui.glade:690
msgid "Velocity Response"
msgstr "Velocidade de resposta"

#: res/microbrute/gui.glade:743
msgid "Keyboard Parameters"
msgstr "Parâmetros de teclado"

#: res/microbrute/gui.glade:781
msgid "Play Mode"
msgstr "Modo de reprodução"

#: res/microbrute/gui.glade:793
msgid "Sequence Retrigger"
msgstr "Acionamento de sequência"

#: res/microbrute/gui.glade:805
msgid "Sequence Change"
msgstr "Mudança de sequência"

#: res/microbrute/gui.glade:817
msgid "Next Step On"
msgstr "Próximo passo"

#: res/microbrute/gui.glade:829
msgid "Step Length"
msgstr "Comprimento de passo"

#: res/microbrute/gui.glade:942
msgid "Sequencer Control"
msgstr "Controle de sequenciador"

#: res/microbrute/gui.glade:980
msgid "LFO Key Retrigger"
msgstr "Acionamento de tecla de LFO"

#: res/microbrute/gui.glade:992
msgid "Envelope Legato"
msgstr "Envelope legato"

#: res/microbrute/gui.glade:1004
msgid "Bend Range"
msgstr "Faixa de bend"

#: res/microbrute/gui.glade:1016
msgid "Gate Length"
msgstr "Comprimento de gate"

#: res/microbrute/gui.glade:1028
msgid "Synchronization"
msgstr "Sincronização"

#: res/microbrute/gui.glade:1122
msgid "Module Parameters"
msgstr "Parâmetros de módulo"
